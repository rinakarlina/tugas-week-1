import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';

const items = [
  {
    src: 'https://i.imgur.com/eXUbvoe.png',
  },
  {
    src: 'https://i.imgur.com/fDMs2Az.png',
  },
  {
    src: 'https://i.imgur.com/suscW43.png',
  }
];

const Slider = () => <UncontrolledCarousel items={items} />;

export default Slider;