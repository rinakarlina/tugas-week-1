import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';
import { Alert } from 'reactstrap';

export default class CreateCampaign extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      content: '',
      name: '',
      isRedirected: false
    }
  }

  handleChangeTitle = (event) => {
    this.setState({
      title: event.target.value
    });
  }

  handleChangeContent = event => {
    this.setState({
      content: event.target.value
    });
  }

  handleChangeName = event => {
    this.setState({
      name: event.target.value
    });
  }

  handleClickSubmit = () => {
    fetch("https://reduxblog.herokuapp.com/api/posts?key=rin",
      {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify({
          title: this.state.title,
          categories: this.state.content,
          content: this.state.name
        })

      })
      .then(() => {
        this.setState({
          isRedirected: true
        });
      })
  }

  render() {
    if (this.state.isRedirected) {
      return (
        <Redirect to="/post-challenges/"></Redirect>
      );
    }
    return (
      <Container className="pl-0 pr-0">
        <Form className="bg-azure p-3 mt-3">
          <FormGroup className="mb-3 mt-3 mr-sm-2 mb-sm-0">
            <Alert color="success">
              Grinain mengajak kamu menjadi Agent of Change dalam melindungi lingkungan di bumi melalui Grinain Campaign.<br></br>
              Share pendapatmu untuk mulai gaya hidup ramah lingkungan yuk.<br></br>
            </Alert>
            <Label for="title">Title</Label>
            <Input onChange={this.handleChangeTitle} type="text" name="title" id="title" placeholder="Add your title" />
          </FormGroup>

          <FormGroup className="mb-3 mt-3 mr-sm-2 mb-sm-0">
            <Label for="content" className="mr-sm-2">Write a Content</Label>
            <Input onChange={this.handleChangeContent} type="textarea" name="content" id="content" placeholder="Tell everyone what your opinion is about" />
          </FormGroup>

          <FormGroup className="mb-3 mt-3 mr-sm-2 mb-sm-0">
            <Label for="name">Tag name</Label>
            <Input onChange={this.handleChangeName} type="text" name="name" id="name" />
          </FormGroup>

          <Button color="success" onClick={this.handleClickSubmit} className="mt-3 mb-3 mr-2">Submit</Button>

          <Link to="/grinain-campaign/">
            <Button className="ml-2">Back</Button>
          </Link>
        </Form>
      </Container>
    );
  }
}