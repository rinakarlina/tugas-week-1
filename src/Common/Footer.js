import React from 'react';
class Footer extends React.Component {
  render() {
    return (
      <div>
        <p className="lead d-flex justify-content-center" style={{ color: "#343a40" }}><br></br>Powered by Rindokarl</p>
      </div>
    );
  }
}
export default Footer;