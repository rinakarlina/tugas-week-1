import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

export default class Header extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div className="logo-container">
        <Navbar style={{ backgroundColor: "#ffffff" }} light expand="md">
          <img style={{ width: "120px" }} src={require('../logogrinain.png')} alt="" />
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/about-us">About Us</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/our-projects/">Our Projects</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/post-challenges/">Post Challenges</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/gallery/">Gallery</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/store/">Store</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/events/">Events</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/competition-awards/">Competition and Awards</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/scholarship-fellowships/">Scholarship and Fellowships</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/volunteer/">Volunteer</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/job-platform/">Jobs</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/tour/">Tour</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/donation/">Donation</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/contact-us/">Contact Us</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

