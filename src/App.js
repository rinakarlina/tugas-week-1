import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from './Common/Header'
import Homepage from './LandingPage/Homepage';
import Show from './About Us/Show';
import Gallery from './Gallery';
import AddCampaign from './AddCampaign';
import OpenClass from './OpenClass';
import CreateCampaign from './CreateCampaign';
import OurProjects from './OurProjects';
import Posts from './Posts';
import ContactUs from './ContactUs'
import Events from './Events';
import Store from './Store';
import CompetitionAwards from './CompetitionAwards';
import ScholarshipFellowships from './ScholarshipFellowships';
import Volunteer from './Volunteer';
import Jobs from './Jobs';
import Tour from './Tour';
import Donation from './Donation';
import Footer from './Common/Footer'

function App() {
  return (
    <div>
      <Router>
        <Header></Header>
        <Route exact path='/' component={Homepage}></Route>
        <Route exact path='/homepage/:name' component={Homepage} ></Route>
        <Route path='/grinain-campaign/' component={AddCampaign}></Route>
        <Route path='/create-campaign' component={CreateCampaign} ></Route>
        <Route path='/grinain-class/' component={OpenClass}></Route>
        <Route path='/about-us' component={Show} ></Route>
        <Route path='/our-projects/' component={OurProjects}></Route>
        <Route path='/gallery' component={Gallery} ></Route>
        <Route path='/post-challenges' component={Posts}></Route>
        <Route path='/events' component={Events} ></Route>
        <Route path='/store/' component={Store} ></Route>
        <Route path='/competition-awards/' component={CompetitionAwards} ></Route>
        <Route path='/scholarship-fellowships/' component={ScholarshipFellowships} ></Route>
        <Route path='/volunteer/' component={Volunteer} ></Route>
        <Route path='/job-platform/' component={Jobs} ></Route>
        <Route path='/tour/' component={Tour} ></Route>
        <Route path='/Donation/' component={Donation} ></Route>
        <Route path='/contact-us/' component={ContactUs} ></Route>
        <Footer></Footer>
      </Router>
    </div>
  );
}

export default App;