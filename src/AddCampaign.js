import React from 'react';
import { Link } from 'react-router-dom';
import { Alert } from 'reactstrap';
import {
  CardImg, Button
} from 'reactstrap';

const AddCampaign = (props) => {
  return (
    <div>
      <Alert color="success"><br></br>
        <p>Ayo, ikutan Grinain Campaign dengan menyuarakan pendapatmu mengenai cara yang bisa kita lakukan untuk menciptakan gaya hidup ramah lingkungan!<br></br>
        Klik tombol "Mulai Aksi" di bawah ini ya Grinainers untuk memulai aksimu.</p>
      </Alert>
      <p className="lead d-flex justify-content-center"><Link to="/create-campaign/"><Button color="success">Mulai Aksi</Button></Link></p>
      <CardImg top width="20%" src="https://i.imgur.com/gIlbS79.png" alt="Card image cap" />
    </div>
  );
};
export default AddCampaign;