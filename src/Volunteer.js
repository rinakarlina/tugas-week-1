import React from 'react';
import { CardImg } from 'reactstrap';

const Volunteer = (props) => {
  return (
    <div className="lead d-flex justify-content-center">
      <div>
            <CardImg src="https://i.imgur.com/i23miAR.png"></CardImg>
      </div>
    </div>
  );
};

export default Volunteer;