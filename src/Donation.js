import React from 'react';
import { CardImg } from 'reactstrap';

const Donation = (props) => {
  return (
    <div className="lead d-flex justify-content-center">
      <div>
            <CardImg src="https://i.imgur.com/vUzUWpY.png"></CardImg>
      </div>
    </div>
  );
};

export default Donation;