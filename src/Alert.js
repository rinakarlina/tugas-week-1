import React from 'react';
import { Alert } from 'reactstrap';

const Alertjs = (props) => {
  return (
    <div>
      <Alert color="success"><br></br>
        <h3 className="alert-heading">Hi, Grinainers!</h3>
        <p>Grinain Class adalah rangkaian kegiatan dari Grinain yang berisi Talkshow Green Lifestyle dan Workshop Mendaur Ulang Jeans Bekas. Dalam kegiatan ini, kamu akan mendapatkan insight mengenai gaya hidup ramah lingkungan sekaligus bagaimana menjadi wirausaha sosial melalui
        cara yang sederhana. Silakan ajukan  kegiatan Grinain Class di wilayahmu melalui kirim email kepada kami di grinain.id@gmail.com <br></br>
          Thank You!</p>
      </Alert>
    </div>
  );
};

export default Alertjs;