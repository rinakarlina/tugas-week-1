import React from 'react';
import { Toast, ToastBody, ToastHeader } from 'reactstrap';
import { CardImg } from 'reactstrap';

const ContactUs = (props) => {
  return (
    <div className="lead d-flex justify-content-center">
      <div>
        <Toast>
          <ToastHeader className="lead d-flex justify-content-center">
            <CardImg src="https://i.imgur.com/hBsbLHT.png" alt="Card image cap" />Let us know if you have any question and collaboration.<br></br>
            Please don't hesitate to contact.
          </ToastHeader>
          <ToastBody>
            Email: grinain.id@gmail.com
          </ToastBody>
        </Toast>
      </div>
    </div>
  );
};

export default ContactUs;