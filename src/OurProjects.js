import React from 'react';
import { Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';

const OurProjects = (props) => {
  return (
    <div style={{ margin: "20px" }}>
      <h1 style={{ backgroundColor: "#29b805" }} light expand="lg" className="lead display-3 justify-content-center">Grinain Projects</h1>
      <p className="lead d-flex justify-content-center">Salah satu misi kami adalah mendorong masyarakat untuk menciptakan gaya hidup yang ramah lingkungan dan peduli sosial, oleh karena itu Grinain mengadakan kampanye kreatif bertema "Green Lifestyle". Selain itu, Grinain juga mengadakan dan menerima kerjasama untuk menyelenggarakan kelas kreatif seperti event Talkshow dan Workshop daur ulang jeans bekas baik bagi perorangan maupun grup atau komunitas serta menjual handmade totebag. Sebagian dari hasil penjualan handmade totebag ini digunakan untuk membantu sesama, terutama kepada orang-orang di sekitar yang membutuhkan.</p>
      <Row className="lead d-flex justify-content-center">
        <Col sm="4">
          <Card body>
            <CardTitle className="lead d-flex justify-content-center">Grinain Campaign</CardTitle>
            <CardText className="lead d-flex justify-content-center">Ayo, ikutan gerakan gaya hidup ramah lingkungan dengan menyuarakan pendapatmu disini!</CardText> <Link to="/create-campaign/" className="lead d-flex justify-content-center"><Button color="success">Mulai Aksi</Button></Link></Card>
        </Col>
        <Col sm="4">
          <Card body>
            <CardTitle className="lead d-flex justify-content-center">Grinain Class</CardTitle>
            <CardText className="lead d-flex justify-content-center">Jadilah bagian dari inisiator kelas Grinain. Daftarkan dirimu dan/ grup komunitasmu yuk!</CardText> <Link to="/grinain-class/" className="lead d-flex justify-content-center"><Button color="success">Buat Kelas</Button></Link></Card>
        </Col>
      </Row>
    </div>
  );
};

export default OurProjects;