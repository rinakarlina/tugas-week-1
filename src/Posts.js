import React from 'react';
import { Table, Container } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Alert } from 'reactstrap'

export default class CreateCampaign extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projectArr: []
    }
  }

  componentDidMount = () => {
    fetch('https://reduxblog.herokuapp.com/api/posts?key=rin')
      .then(response => response.json())
      .then(data => {
        /*
        if (this.state.projectArr[0] === null) {
          this.setState(state => ({
            projectArr: [...data]
          }));
        }
        */
        this.setState(state => ({
          projectArr: [...state.projectArr, ...data]
        }))
      })
  }

  render() {
    return (
      <Container className="pl-0 pr-0">
        <Alert color="success">
          Mission to inspire people to raise awareness about green lifestyle. Are you ready, Grinainers?<br></br> #GrinainCampaign
        </Alert>
        <Table>
          <thead>
            <tr>
              <th>id</th>
              <th>Title</th>
              <th>Content</th>
              <th>Tag Name</th>
            </tr>
          </thead>
          <tbody>
            {this.state.projectArr.map(project => {
              return (
                <tr key={project.id}>
                  <th scope="row">{project.id}</th>
                  <td><Link to={`/posts${project.id}`}>{project.title}</Link></td>
                  <td>{project.categories}</td>
                  <td>{project.content}</td>
                </tr>
              );
            })
            }
          </tbody>
        </Table>

      </Container>
    );
  }
}