import React from 'react';
import { Media } from 'reactstrap';
const Mediajs = () => {
  return (
    <div>
      <Media style={{ margin: "15px" }}>
        <Media left top href="/grinain-campaign/">
          <img style={{ width: "100px" }} src="https://i.imgur.com/gIlbS79.png" alt="#GrinainCampaign" />
        </Media>
        <Media body style={{ margin: "11px" }}>
          <Media heading style={{ margin: "2px" }}>
            Grinain Campaign
          </Media>
          Share pendapatmu untuk menunjukkan dukunganmu dalam gerakan gaya hidup ramah lingkungan! <br></br>
          #GrinainCampaign
        </Media>
      </Media>
      <Media style={{ margin: "15px" }} className="mt-1">
        <Media left middle href="/grinain-class/">
          <img style={{ width: "100px" }} src="https://i.imgur.com/88ElrEY.png" alt="#GrinainClass" />
        </Media>
        <Media body style={{ margin: "11px" }}>
          <Media heading style={{ margin: "2px" }}>
            Grinain Class
          </Media>
          Yuk, ikutan menjadi bagian dari solusi permasalahan sekitar dengan memanfaatkan barang bekas bersama kami! <br></br>
          #GrinainClass
        </Media>
      </Media>
    </div>
  );
};

export default Mediajs;