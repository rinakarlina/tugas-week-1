import React from 'react'
import Slider from './Slider'
import { Badge } from 'reactstrap';
const Gallery = () => {
    return (
        <div style={{ height: "auto" }} fluid>
            <h1 className="lead d-flex justify-content-center">
                ~ OUR GALLERY ~<Badge color="secondary"></Badge></h1>
            <Slider></Slider>
        </div>
    )

}

export default Gallery;