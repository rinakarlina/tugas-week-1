import React from 'react'
import { Container, Row, Col, ListGroup, ListGroupItem, TabContent, TabPane, Nav, NavItem, NavLink, Card, CardTitle, CardText } from 'reactstrap';
import classnames from 'classnames';

export default class Show extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
      project: {}
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }

  }
  render() {
    return (
      <Container>
        <br></br>
        <Row>
          <Col sm='4'>
            <ListGroup>
              <ListGroupItem className="justify-content-between"> <img className='imageProfile' src="https://i.imgur.com/SumDQEI.png" width="310px" alt="" /></ListGroupItem>
            </ListGroup>
          </Col>
          <Col sm='8'>
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '1' })}
                  onClick={() => { this.toggle('1'); }}
                >
                  Sejarah
            </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '2' })}
                  onClick={() => { this.toggle('2'); }}
                >
                  Visi Misi
            </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '3' })}
                  onClick={() => { this.toggle('3'); }}
                >
                  Tagline
            </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
                <Row>
                  <Col sm="12">
                    <h3>Introduction</h3>
                    <p>Pada awalnya "Grinain" hadir sebagai proyek sosial dari Rindokarl. Grinain didirikan oleh seorang founder, Rina Karlina pada bulan September 2018. Grinain (Gerakan Lestari Lingkungan Indonesia) adalah platform untuk meningkatkan kapasitas masyarakat di seluruh Indonesia akan isu lingkungan dengan membagikan berbagai informasi seperti events, volunteer, competition &amp; awards, jobs, scholarships &amp; fellowships, CSR, donation terkait bidang lingkungan. <br></br><br></br>
                      Grinain mempunyai project yaitu sebagai ruang untuk edukasi dalam memanfaatkan barang bekas dengan cara yang kreatif dan menyenangkan yang mengajak generasi muda untuk turut ikut aktif berwirausaha sekaligus mewujudkan lingkungan hidup yang berkelanjutan. Grinain juga mengadakan Grinain Campaign untuk mengajak semua orang berbuat sesuatu terhadap lingkungan. Selain itu, kedepannya Grinain sebagai salah satu brand fashion upcycle yang menyelamatkan jeans bekas yang tidak terpakai dan diubah menjadi produk fashion seperti tas jeans yang keren. <br></br><br></br>
                      Selama perjalanannya hingga sekarang, Grinain membuka kelas kreatif kepada masyarakat berupa talkshow dan workshop mendaur ulang jeans bekas menjadi totebag, pouch maupun slingbag kreatif. Tujuan diadakannya kegiatan ini adalah membuat produk dengan cara mendaur ulang jeans bekas menjadi totebag/pouch/slingbag atau tas yang bisa digunakan setiap harinya, dapat memberikan value pada produk bahwa dengan menggunakannya menjadi bagian dari solusi permasalahan sekitar yaitu limbah pakaian dan dapat meminimalisir keberadaan sampah plastik, meningkatkan kreativitas generasi muda dalam pemanfaatan sampah pakaian seperti jeans bekas, mengembangkan jiwa wirausaha dan kemandirian, serta menambah pengalaman dan pengetahuan tentang pemanfaatan sampah pakaian menjadi barang yang berguna dan memiliki nilai ekonomis.</p>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
                <Row>
                  <Col sm="6">
                    <Card body>
                      <CardTitle>VISION :</CardTitle>
                      <CardText>Menciptakan lingkungan hidup yang berkelanjutan dan perubahan sosial di Indonesia bahkan di dunia.</CardText>
                    </Card>
                  </Col>
                  <Col sm="6">
                    <Card body>
                      <CardTitle>MISSION :</CardTitle>
                      <CardText>1. Menyelesaikan isu mengenai lingkungan.<br></br>
                        2. Mendorong semakin banyak industri dan bisnis di bidang industri kreatif maupun bidang lainnya yang peduli akan lingkungan.<br></br>
                        3. Meningkatkan kesadaran masyarakat akan kerugian yang ditimbulkan dari dampak kerusakan lingkungan, baik dari segi ekonomi, lingkungan dan sosial, melalui layanan Grinain.<br></br>
                        4. Mendorong masyarakat untuk menciptakan gaya hidup yang ramah lingkungan dan peduli sosial.</CardText>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="3">
                <Row>
                  <Col sm="12">
                    <br></br>
                    <h1>"Zona Menyelamatkan Bumi."</h1>

                  </Col>
                </Row>
              </TabPane>
            </TabContent>

          </Col>
        </Row>

      </Container>
    );
  }
}