import React from 'react'
import { Jumbotron, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Badge } from 'reactstrap';
import { Alert } from 'reactstrap';
import Media from '../Media'

export default class Homepage extends React.Component {

    render() {
        return (
            <div>
                <Jumbotron style={{ backgroundColor: "#29b805" }} light expand="lg">
                    <h3 className="lead d-flex justify-content-center display-2" style={{ color: "#ffffff" }}>
                        One Planet, One Vision.</h3>
                    <h5 className="lead d-flex justify-content-center display-4" style={{ color: "#ffffff" }}>"Cari apa saja seputar lingkungan langsung di situs grinain."</h5><br></br>
                    <p className="lead d-flex justify-content-center">
                        <Link to='/about-us/'>
                            <Button outline color="warning" size="lg">See More</Button>
                        </Link> </p>
                </Jumbotron>
                <h1 className="d-flex justify-content-center">Our Projects<Badge color="secondary"></Badge></h1>
                <Alert color="success"><br></br>
                    <p>Bahagia itu ada dalam tindakan yang sederhana, ketika kita saling bersinergi untuk berkontribusi terhadap lingkungan sekitar. -Rina Karlina-</p>
                </Alert>
                <Media></Media>
            </div>
        )
    }
}